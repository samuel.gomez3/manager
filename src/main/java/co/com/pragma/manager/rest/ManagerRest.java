package co.com.pragma.manager.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import co.com.pragma.manager.business.IManagerBusiness;
import co.com.pragma.manager.domain.AnalysedProject;

@RestController
@RequestMapping("/api/v1")
public class ManagerRest {

	@Autowired
	private IManagerBusiness managerBusiness;

	@GetMapping("/health")
	public ResponseEntity<HttpStatus> health() {
		return ResponseEntity.ok().body(HttpStatus.OK);
	}

	@GetMapping("/analysis")
	public ResponseEntity<List<AnalysedProject>> analysis(@RequestParam("route") String route) {
		return managerBusiness.projectAnalysis(route);
	}

}
