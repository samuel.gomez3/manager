package co.com.pragma.manager.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AnalysedDependency {
	
	String dependency;
	String mavenVersion;
	String pomVersion;

}
