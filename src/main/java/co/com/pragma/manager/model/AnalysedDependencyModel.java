package co.com.pragma.manager.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AnalysedDependencyModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	long id;
	String dependency;
	String mavenVersion;
	String pomVersion;
	@ManyToOne
	AnalysedProjectModel analysedProject;
	
	public AnalysedDependencyModel(AnalysedDependencyModel dependecy) {
		this.id = dependecy.getId();
		this.dependency = dependecy.getDependency();
		this.mavenVersion = dependecy.getMavenVersion();
		this.pomVersion = dependecy.getPomVersion();
	}

}
