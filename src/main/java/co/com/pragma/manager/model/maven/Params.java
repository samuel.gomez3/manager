package co.com.pragma.manager.model.maven;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Params {

    public String q;
    public String core;
    public String defType;
    public String qf;
    public String indent;
    public String spellcheck;
    public String fl;
    public String start;
    public String sort;
    public String count;
    public String rows;
    public String wt;
    public String version;

}
