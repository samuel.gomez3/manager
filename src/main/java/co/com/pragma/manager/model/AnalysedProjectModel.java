package co.com.pragma.manager.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AnalysedProjectModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	long id;
	String route;
	@Transient
	List<AnalysedDependencyModel> dependecies;

}
