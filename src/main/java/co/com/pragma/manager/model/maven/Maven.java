package co.com.pragma.manager.model.maven;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Maven {
	
	public ResponseHeader responseHeader;
	public Response response;
	public Spellcheck spellcheck;

}
