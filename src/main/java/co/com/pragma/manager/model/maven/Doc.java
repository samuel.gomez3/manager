package co.com.pragma.manager.model.maven;

import java.util.ArrayList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@Entity(name = "MAVEN_DEPENDENCY")
public class Doc {

	@Id
	public String id;
	@Column(name="groupId")
	public String g;
	@Column(name="artifactId")
	public String a;
	public String latestVersion;
	@Transient
	public String repositoryId;
	@Column(name="packaging")
	public String p;
	@Column(name="version")
	public String v;
	@Transient
	public Object timestamp;
	public int versionCount;
	@Transient
	public ArrayList<String> text;
	@Transient
	public ArrayList<String> ec;
	int error;

	public Doc(String a) {
		this.a = a;
	}

	@Override
	public boolean equals(Object obj) {
		Doc doc = (Doc) obj;
		if (doc.a.equals(a)) {
			return true;
		}
		return false;
	}
	
}
