package co.com.pragma.manager.model.pom;

import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Dependencies {
	
    public ArrayList<Dependency> dependency;
    
}
