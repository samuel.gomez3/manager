package co.com.pragma.manager.model.maven;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ResponseHeader {
	
    public int status;
    public int QTime;
    public Params params;
}
