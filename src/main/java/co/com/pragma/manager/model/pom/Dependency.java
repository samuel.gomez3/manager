package co.com.pragma.manager.model.pom;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Dependency {
	
	public String groupId;
	public String artifactId;
	public String scope;
	public String version;
	public boolean optional;

	@Override
	public boolean equals(Object obj) {
		Dependency dependency = (Dependency) obj;
		if (dependency.artifactId.equals(artifactId)) {
			return true;
		}
		return false;
	}
	
}
