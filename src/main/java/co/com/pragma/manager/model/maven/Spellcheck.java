package co.com.pragma.manager.model.maven;

import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Spellcheck {
	
	public ArrayList<Object> suggestions;
	
}
