package co.com.pragma.manager.business.impl;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import co.com.pragma.manager.business.IManagerBusiness;
import co.com.pragma.manager.domain.AnalysedDependency;
import co.com.pragma.manager.domain.AnalysedProject;
import co.com.pragma.manager.model.maven.Doc;
import co.com.pragma.manager.model.maven.Maven;
import co.com.pragma.manager.model.pom.Dependencies;
import co.com.pragma.manager.model.pom.Dependency;
import co.com.pragma.manager.repository.AnalysedDependencyRepository;
import co.com.pragma.manager.repository.AnalysedProjectRepository;
import co.com.pragma.manager.repository.MavenDependencyRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ManagerBusinessImpl implements IManagerBusiness {

	private static final Gson gson = new Gson();
	private static final String LOG_START_ANALYSIS = "Start of analysis of route {}";
	private static final String LOG_COMPLETE_ANALYSIS = "Analysis of route {} has been completed";
	private static final String LOG_ERROR = "Error: {}";
	private static final String LOG_WARN = "Warn: {}";
	private static final String LOG_SAVE_DEPENDENCY = "Save dependency {}";
	private static final String LOG_Analysed_DEPENDENCY = "Analysed dependency {}";
	private static final String LOG_GET_ROUTES_POM = "Getting routes from the pom";
	private static final String LOG_END_SHEDULED = "Scheduled End";
	private static final String LOG_START_SHEDULED = "Start scheduled";
	private static final String LOG_GET_DEPENDENCIES_POM = "Obtaining pom {} dependencies";
	private static final String LOG_QUERYING_DEPENDENCY_MAVEN = "Querying dependency {} in maven";
	private static final String FILENAME_MAVEN_DEPENDENCIES = "pom.xml";
	private static final String EXCLUDE_FOLDER_FILE_MAVEN_DEPENDENCY = "target";
	private static final String GET = "GET";
	private static final String OPEN_DEPENDENCY_MAVEN = "<dependencies>";
	private static final String CLOSE_DEPENDENCY_MAVEN = "</dependencies>";
	private static final String OPEN_DEPENDENCY_MANAGEMENT_MAVEN = "<dependencyManagement>";

	@Autowired
	AnalysedDependencyRepository analysedDependencyRepository;
	@Autowired
	AnalysedProjectRepository analysedProjectRepository;
	@Autowired
	MavenDependencyRepository mavenDependencyRepository;

	public ResponseEntity<List<AnalysedProject>> projectAnalysis(String route) {
		return new ResponseEntity<List<AnalysedProject>>(projectAnalysisService(route), HttpStatus.OK);
	}

	private List<AnalysedProject> projectAnalysisService(String route) {
		log.info(LOG_START_ANALYSIS, route);
		List<AnalysedProject> projects = new ArrayList<AnalysedProject>();
		try {
			List<String> rututePom = readPoms(route);
			for (String ruotePom : rututePom) {
				List<AnalysedDependency> dependenciesProject = new ArrayList<AnalysedDependency>();
				Dependencies dependencies = getDependenciesPom(ruotePom);
				if (dependencies != null) {
					if (dependencies.dependency != null) {
						for (Dependency dependency : dependencies.dependency) {
							List<Doc> docs = getDependencyCache(dependency.groupId, dependency.artifactId);
							String v;
							if (docs.size() == 0) {
								v = null;
							} else {
								v = docs.get(0).v;
							}
							dependenciesProject
									.add(new AnalysedDependency(dependency.artifactId, v, dependency.version));
							log.info(LOG_Analysed_DEPENDENCY, dependency.artifactId);
						}
					}
				}
				projects.add(new AnalysedProject(ruotePom, dependenciesProject));
			}
		} catch (Exception e) {
			log.error(LOG_ERROR, e);
		}
		log.info(LOG_COMPLETE_ANALYSIS, route);
		return projects;
	}

	private List<Doc> getDependencyCache(String g, String a) {

		List<Doc> docs = mavenDependencyRepository.findByAAndG(a, g);

		if (docs != null) {
			if (docs.size() != 0) {
				return docs;
			}
		}
		return getDependenciesMaven(g, a);
	}

	List<String> readPoms(String route) {
		log.info(LOG_GET_ROUTES_POM);
		List<String> routePom = new ArrayList<>();
		try {
			SimpleFileVisitor<Path> file = new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path filePath, BasicFileAttributes attrs) throws IOException {
					if (filePath.toString().contains(FILENAME_MAVEN_DEPENDENCIES)
							&& !filePath.toString().contains(EXCLUDE_FOLDER_FILE_MAVEN_DEPENDENCY)) {
						routePom.add(filePath.toString());
					}
					return FileVisitResult.CONTINUE;
				}
			};
			Files.walkFileTree(Paths.get(route), file);
		} catch (IOException e) {
			log.error(LOG_ERROR, e);
		}
		return routePom;
	}

	Dependencies getDependenciesPom(String route) {
		log.info(LOG_GET_DEPENDENCIES_POM, route);
		Dependencies dependencies = new Dependencies();
		try {
			Scanner file = new Scanner(new File(route));
			String pom = "";
			while (file.hasNextLine()) {
				pom += file.nextLine();
			}
			String[] part = pom.split(OPEN_DEPENDENCY_MAVEN);
			String[] part2;
			if (!part[0].contains(OPEN_DEPENDENCY_MANAGEMENT_MAVEN)) {
				part2 = part[1].split(CLOSE_DEPENDENCY_MAVEN);
			} else {
				part2 = part[2].split(CLOSE_DEPENDENCY_MAVEN);
			}
			JSONObject json = XML.toJSONObject(part2[0]);
			file.close();
			dependencies = gson.fromJson(json.toString(), Dependencies.class);
		} catch (Exception e) {
			log.error(LOG_ERROR, e);
		}
		return dependencies;
	}

	List<Doc> getDependenciesMaven(String g, String a) {
		log.info(LOG_QUERYING_DEPENDENCY_MAVEN, a);
		String urlString = "https://search.maven.org/solrsearch/select?q=g:" + g + "+AND+a:" + a + "&core=gav&wt=json";
		List<Doc> docs = new ArrayList<Doc>();
		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod(GET);
			connection.connect();
			StringBuilder builder = new StringBuilder();
			Scanner scanner = new Scanner(url.openStream());
			while (scanner.hasNext())
				builder.append(scanner.nextLine());
			scanner.close();
			docs = gson.fromJson(builder.toString(), Maven.class).response.docs;
			if (docs.size() == 0) {
				docs.add(new Doc(g + a, g, a, "1", null, null, null, null, 0, null, null, 0));
			}
			boolean b = true;
			for (Doc doc : docs) {
				if (b) {
					doc.latestVersion = "1";
					b = false;
				} else {
					doc.latestVersion = "0";
				}
				mavenDependencyRepository.save(doc);
				log.info(LOG_SAVE_DEPENDENCY, doc.id);
			}
			return docs;
		} catch (ConnectException e) {
			log.warn(LOG_WARN + " " + urlString, e.toString());
			return getDependenciesMaven(g, a);
		} catch (SocketTimeoutException e) {
			log.warn(LOG_WARN + " " + urlString, e.toString());
			return getDependenciesMaven(g, a);
		} catch (IOException e) {
			log.error(LOG_ERROR, e.toString());
			return getDependenciesMaven(g, a);
		}
	}

	@Scheduled(cron = "0 0 0/12 ? * *")
	public void updateDependencies() {
		log.info(LOG_START_SHEDULED);
		List<Doc> newDocs = new ArrayList<Doc>();
		for (Doc doc : mavenDependencyRepository.findByLatestVersion("1")) {
			for (Doc doc2 : getDependenciesMaven(doc.g, doc.a)) {
				newDocs.add(doc2);
			}
		}
		log.info(LOG_END_SHEDULED);
	}

}