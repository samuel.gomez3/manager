package co.com.pragma.manager.business;

import java.util.List;
import org.springframework.http.ResponseEntity;
import co.com.pragma.manager.domain.AnalysedProject;

public interface IManagerBusiness {

	ResponseEntity<List<AnalysedProject>> projectAnalysis(String route);

}
