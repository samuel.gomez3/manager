package co.com.pragma.manager;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
@EnableScheduling
public class ManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManagerApplication.class, args);
	}

	@PostConstruct
	public void logInicio() {
		String version = this.getClass().getPackage().getImplementationVersion();
		log.info("Start  manager version {}", version);
	}

	@PreDestroy
	public void logFinal() {
		log.info("Stopping...");
	}
	
}
