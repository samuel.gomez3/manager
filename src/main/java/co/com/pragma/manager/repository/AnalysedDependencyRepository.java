package co.com.pragma.manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import co.com.pragma.manager.model.AnalysedDependencyModel;

public interface AnalysedDependencyRepository extends JpaRepository<AnalysedDependencyModel, Long> {

}
