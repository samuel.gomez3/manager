package co.com.pragma.manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import co.com.pragma.manager.model.AnalysedProjectModel;

public interface AnalysedProjectRepository extends JpaRepository<AnalysedProjectModel, Long> {

}
