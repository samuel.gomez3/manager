package co.com.pragma.manager.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import co.com.pragma.manager.model.maven.Doc;

public interface MavenDependencyRepository extends JpaRepository<Doc, String> {

	List<Doc> findByLatestVersion(String latestVersion);
	List<Doc> findByAAndG(String a, String g);

}
